Pod::Spec.new do |s|
    
    s.name             = 'DVNTAlamofireWrapper'
    s.version          = '2.3.13'
    s.summary          = 'An amazing Alamofire wrapper.'
    s.description      = 'A wrapper to use Amalofire easily.'
    s.homepage         = 'https://www.devinet.es'
    s.license          = { :type => 'Copyright (c) 2021 Devinet 2013, S.L.U.', :file => 'LICENSE' }
    s.author           = { 'Raúl Vidal Muiños' => 'contacto@devinet.es' }
    s.social_media_url = 'https://twitter.com/devinet_es'
    
    s.ios.deployment_target = "10.0"
    s.tvos.deployment_target  = "10.0"
    
    s.swift_versions   = ['3.0', '4.0', '4.1', '4.2', '5.0', '5.1', '5.2']
    s.source           = { :git => 'https://bitbucket.org/Devinet_Team/ios-library-dvntalamofirewrapper.git', :tag => s.version.to_s }
    s.frameworks       = 'UIKit'
    s.source_files     = 'Sources/DVNTAlamofireWrapper/Classes/**/*'
    s.exclude_files    = 'Sources/DVNTAlamofireWrapper/**/*.plist'
    
    
    s.dependency 'Alamofire', '~>5.4.3'
    s.dependency 'SwiftyJSON', '~>5.0.1'
    s.dependency 'ReachabilitySwift', '~>5.0.0'
    
end
