Pod::Spec.new do |s|
    
    s.name             = 'DVNTAlamofireWrapper'
    s.version          = '1.0.2'
    s.summary          = 'An amazing Alamofire wrapper.'
    s.description      = 'A wrapper to use Amalofire easily.'
    s.homepage         = 'https://www.devinet.es'
    s.license          = { :type => 'Copyright (c) 2019 Devinet 2013, S.L.U.', :file => 'LICENSE' }
    s.author           = { 'Raúl Vidal Muiños' => 'contacto@devinet.es' }
    s.social_media_url = 'https://twitter.com/devinet_es'
    s.platform         = :ios, '9.3'
    s.ios.deployment_target = '9.3'
    s.swift_versions   = ['3.0', '4.0', '4.1', '4.2', '5.0']
    s.source           = { :git => 'https://bitbucket.org/Devinet_Team/ios-library-dvntalamofirewrapper.git', :tag => s.version.to_s }
    s.frameworks       = 'UIKit'
    s.source_files     = 'DVNTAlamofireWrapper/Classes/**/*'
    s.exclude_files    = 'DVNTAlamofireWrapper/**/*.plist'
    
    s.dependency 'Alamofire', '~>4.8.2'
    s.dependency 'SwiftyJSON', '~>4.0'
    s.dependency 'ReachabilitySwift', '~>4.3.1'
    
end
