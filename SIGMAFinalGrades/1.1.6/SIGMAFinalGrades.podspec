Pod::Spec.new do |s|
  
  s.name             = 'SIGMAFinalGrades'
  s.version          = '1.1.6'
  s.summary          = 'Final grades module for SIGMA.'
  s.description      = 'A module to access to the final grades of a user.'
  s.homepage         = 'https://www.sigmaaie.org'
  s.license          = { :type => 'Copyright (c) 2019 SIGMA, GESTIÓN UNIVERSITARIA, AIE', :file => 'LICENSE' }
  s.author           = { 'Raúl Vidal Muiños' => 'contacto@devinet.es' }
  s.social_media_url = 'https://twitter.com/devinet_es'
  s.platform         = :ios, '13.4'
  s.ios.deployment_target = '11.0'
  s.swift_versions   = ['4.0', '4.1', '4.2', '5.0', '5.1']
  s.source           = { :git => 'https://bitbucket.org/Devinet_Team/module-sigmafinalgrades.git', :tag => s.version.to_s }
  s.frameworks       = 'UIKit'
  s.source_files     = 'SIGMAFinalGrades/Classes/**/*'
  s.exclude_files    = 'SIGMAFinalGrades/**/*.plist'
  s.resource_bundles = { 'SIGMAFinalGrades' => ['SIGMAFinalGrades/Assets/**/*.{strings,xib,xcassets,storyboard,imageset}'] }
  
  s.dependency 'Tags', '~>0.3.1'
  s.dependency 'Klendario', '~>1.0.1'
  s.dependency 'SwiftyJSON', '~>5.0.0'
  s.dependency 'PopupDialog', '~>1.1.1'
  s.dependency 'CryptoSwift', '~>1.3.1'
  s.dependency 'DVNTAlertManager', '~>1.1.2'
  s.dependency 'DVNTAlamofireWrapper', '~>2.1.0'
end
